//Creating websocket connection 
let socket = new WebSocket("ws://localhost:8080/")
console.log("Attempting to connect to the main menu")

let pin = null
let clientId = null


function makeStruct(names) {
    var names = names.split(' ');
    var count = names.length;
    function constructor() {
      for (var i = 0; i < count; i++) {
        this[names[i]] = arguments[i];
      }
    }
    return constructor;
}

//Closing connection
socket.addEventListener("close", function onSocketClose(event) {
    console.log("Disconnected from server")
})


socket.addEventListener('open', function onSocketOpen(event){
    let userInfo = sessionStorage.getItem("userInfo")
    if(userInfo){
        console.log("Welcome back "+userInfo.name)
        console.log("Game "+userInfo.pin+" is still active.")
        if(userInfo.type == "host"){
            console.log("Players are waiting for you.")
        }else{
            console.log("Fight for the first place.")
        }
        return
    }
    console.log("Welcome to quizzes online")
})

socket.addEventListener('message', function handleMessage(event){
    let response = JSON.parse(event.data)

    let method = response.Method
    let error = response.Error
    clientId = response.ClientId
    pin = response.Pin
    //Created a server
    if(method === "create"){
        if(error === ""){
            let UserInfo = makeStruct("clientId type pin name")
            let userInfo = new UserInfo(clientId, "host", pin, "")

            sessionStorage.setItem("userInfo", JSON.stringify(userInfo))
            closeSocket(null)


            let path = window.location.pathname 
            path = path.substring(0, path.lastIndexOf('/'));
            path = path.substring(0, path.lastIndexOf('/'));
            window.location.href = path+"/hostMain/hostMain.html"
        }else{
            console.log(method)
        }
    }
})


// Closes the websocket
const closeSocket = () => {
    const payLoad = {
        "method": "disconnect",
        "clientId": clientId
    }
    socket.send(JSON.stringify(payLoad));
}

//Closes websocket cleanly if window is closed
window.onbeforeunload = function(){
    closeSocket()
}