//Creating websocket connection 
let socket = new WebSocket("ws://localhost:8080/")
console.log("Attempting to connect to the host menu")

let pin = null
let clientId = null


function makeStruct(names) {
    var names = names.split(' ');
    var count = names.length;
    function constructor() {
      for (var i = 0; i < count; i++) {
        this[names[i]] = arguments[i];
      }
    }
    return constructor;
}

//Closing connection
socket.addEventListener("close", function onSocketClose(event) {
    console.log("Disconnected from server")
})


socket.addEventListener('open', function onSocketOpen(event){
    let userInfo = JSON.parse(sessionStorage.getItem("userInfo"))
    console.log({userInfo})
    if(!userInfo){
        console.log("You should have user info by now")
        return
    }
    clientId = userInfo.clientId
    pin = userInfo.pin
    console.log("Welcome to the host menu "+userInfo.clientId)
    if(userInfo.type != "host"){
        console.log("You are a player and should not be here")
        return
    }

    let d = document.getElementById("pinNumber")
    let h = document.createElement("h1")
    h.textContent = pin
    d.appendChild(h)
})

socket.addEventListener('message', function handleMessage(event){
    let response = JSON.parse(event.data)

    let method = response.Method
    let error = response.Error
    clientId = response.ClientId
    pin = response.Pin
    //Created a server
    if(method === "create"){
        if(error === ""){
            let UserInfo = makeStruct("clientId type pin name")
            let userInfo = new UserInfo(clientId, "host", pin, "")

            sessionStorage.setItem("userInfo", JSON.stringify(userInfo))
            closeSocket(null)


            let path = window.location.pathname 
            path = path.substring(0, path.lastIndexOf('/'));
            path = path.substring(0, path.lastIndexOf('/'));
            window.location.href = path+"/hostMain/hostMain.html"
        }else{
            console.log(method)
        }
    }
})


// Closes the websocket
const closeSocket = () => {
    const payLoad = {
        "method": "disconnect",
        "clientId": clientId
    }
    socket.send(JSON.stringify(payLoad));
}

//Closes websocket cleanly if window is closed
window.onbeforeunload = function(){
    closeSocket()
}