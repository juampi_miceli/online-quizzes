function createNewGameComm(socket){
    payLoad = {
        "method": "create"
    }
    socket.send(JSON.stringify(payLoad))
}

function joinNewGameComm(socket, pin){
    payLoad = {
        "method": "disconnect",
        "pin": pin
    }

    socket.send(JSON.stringify(payLoad))
}