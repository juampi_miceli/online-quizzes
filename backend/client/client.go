package client

type Client struct {
	id           string
	nick         string
	clientType   string
	currentLobby string
}

func NewClient(id string, clientType string) Client {
	client := Client{}
	client.id = id
	client.clientType = clientType
	return client
}

func (client *Client) SetNick(nick string) {
	client.nick = nick
}

func (client *Client) GetNick() string {
	return client.nick
}

func (client *Client) GetId() string {
	return client.id
}

func (client *Client) GetClientType() string {
	return client.clientType
}

func (client *Client) SetCurrentLobby(currentLobby string) {
	client.currentLobby = currentLobby
}

func (client *Client) GetCurrentLobby() string {
	return client.currentLobby
}
