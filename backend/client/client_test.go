package client

import (
	"math/rand"
	"testing"
)

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-1234567890"

func RandomNick(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func TestNewClient(t *testing.T) {
	client := NewClient("1234", "host")
	if client.GetId() != "1234" {
		t.Errorf("Expected 1234. Got \"%v\"", client.GetId())
	}
	if client.GetClientType() != "host" {
		t.Errorf("Expected \"host\". Got \"%v\"", client.GetClientType())
	}
}

func TestSettersAndGetters(t *testing.T) {
	client := NewClient("1234", "host")
	if client.GetNick() != "" {
		t.Errorf("Expected \"\". Got \"%v\"", client.GetNick())
	}
	for i := 0; i < 1000; i++ {
		randomString := RandomNick(i)
		client.SetNick(randomString)
		if client.GetNick() != randomString {
			t.Errorf("Expected \"%v\". Got \"%v\"", randomString, client.GetNick())
		}

		client.SetCurrentLobby(randomString)
		if client.GetCurrentLobby() != randomString {
			t.Errorf("Expected \"%v\". Got \"%v\"", randomString, client.GetCurrentLobby())
		}
	}
}
