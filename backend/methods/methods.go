package methods

import (
	"gitlab.com/juampi_miceli/online-quizzes/backend/response"
)

func CreateLobbyResponse(lobbyPin string, hostId string) response.Response {

	response := response.NewResponse("create", "", lobbyPin, hostId)

	return response
}
