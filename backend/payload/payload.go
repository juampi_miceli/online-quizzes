package payload

type PayLoad struct {
	Method string `json:"method"`
	Pin    string `json:"pin"`
}
