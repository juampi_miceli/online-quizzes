package response

type Response struct {
	Method   string
	Error    string
	Pin      string
	ClientId string
}

func NewResponse(method string, err string, pin string, clientId string) Response {
	return Response{method, err, pin, clientId}
}
