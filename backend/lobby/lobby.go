package lobby

import "gitlab.com/juampi_miceli/online-quizzes/backend/client"

type Lobby struct {
	pin     string
	host    client.Client
	players []client.Client
}

func NewLobby(pin string, host *client.Client) Lobby {
	return Lobby{pin, *host, make([]client.Client, 0)}
}

func (lobby *Lobby) GetPin() string {
	return lobby.pin
}

func (lobby *Lobby) GetHost() client.Client {
	return lobby.host
}

func (lobby *Lobby) GetPlayers() []client.Client {
	return lobby.players
}

func (lobby *Lobby) nickAlreadyPresent(client *client.Client) bool {
	newNick := (*client).GetNick()
	if lobby.host.GetNick() == newNick {
		return true
	}
	for _, player := range lobby.players {
		if player.GetNick() == newNick {
			return true
		}
	}
	return false
}

func (lobby *Lobby) AddPlayer(client *client.Client) bool {
	if lobby.nickAlreadyPresent(client) {
		return false
	}
	(*lobby).players = append((*lobby).players, *client)
	return true
}
