package safestack

import (
	"fmt"
	"sync"
)

type Stack struct {
	values []interface{}
	mutex  sync.Mutex
}

func NewStack() Stack {
	return Stack{}
}

func (s *Stack) Push(elem interface{}) {
	s.mutex.Lock()
	s.values = append(s.values, elem)
	s.mutex.Unlock()
}

func (s *Stack) Empty() bool {
	s.mutex.Lock()
	res := len(s.values) == 0
	s.mutex.Unlock()
	return res
}

func (s *Stack) Pop() interface{} {
	if !s.Empty() {
		s.mutex.Lock()
		n := len(s.values) - 1
		res := (s.values)[n]
		(s.values)[n] = nil
		(s.values) = (s.values)[:n]
		s.mutex.Unlock()

		return res
	}

	return nil
}

func (s *Stack) NewArange(low int, high int) {
	for !(*s).Empty() {
		(*s).Pop()
	}
	for i := low; i < high; i++ {
		(*s).Push(fmt.Sprintf("%04v", i))
	}
}
