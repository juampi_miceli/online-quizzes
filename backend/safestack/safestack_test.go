package safestack

import "testing"

func TestArange(t *testing.T) {
	stack := NewStack()
	stack.NewArange(0, 10000)
	val := stack.Pop()
	if val != "9999" {
		t.Errorf("Expected %v. Got %v", "9999", val)
	}

	stack.NewArange(0, 10)
	val = stack.Pop()
	if val != "0009" {
		t.Errorf("Expected %v. Got %v", "0009", val)
	}
	for i := 0; i < 11; i++ {
		stack.Pop()
	}
	if stack.Empty() == false {
		t.Errorf("Stack should be empty, instead there are %v elements", len(stack.values))
	}

	stack.Push("1234")
	val = stack.Pop()
	if val != "1234" {
		t.Errorf("Expected %v. Got %v", "1234", val)
	}
}
