package main

import (
	"fmt"
	"log"
	"net/http"

	"encoding/json"

	"github.com/gorilla/websocket"
	"gitlab.com/juampi_miceli/online-quizzes/backend/client"
	"gitlab.com/juampi_miceli/online-quizzes/backend/lobby"
	"gitlab.com/juampi_miceli/online-quizzes/backend/methods"
	"gitlab.com/juampi_miceli/online-quizzes/backend/payload"
	"gitlab.com/juampi_miceli/online-quizzes/backend/response"
	"gitlab.com/juampi_miceli/online-quizzes/backend/safestack"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

var validPins safestack.Stack
var validIds safestack.Stack

var lobbys []lobby.Lobby

func writeToClient(conn *websocket.Conn, messageType int, response *response.Response) {
	jsonResponse, error := json.Marshal(response)
	if error != nil {
		fmt.Println(error)
	}
	conn.WriteMessage(messageType, jsonResponse)
}

func reader(conn *websocket.Conn) {
	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			fmt.Println(err)
		}

		response := response.Response{}
		payLoad := payload.PayLoad{}
		json.Unmarshal(p, &payLoad)

		log.Println(payLoad)

		if payLoad.Method == "disconnect" {

			err = conn.Close()
			if err != nil {
				fmt.Println(err)
			}
			fmt.Println("Connection closed")
			return
		}

		if payLoad.Method == "create" {
			pin := validPins.Pop().(string)
			clientId := validIds.Pop().(string)
			host := client.NewClient(clientId, "host")
			lobby := lobby.NewLobby(pin, &host)

			lobbys = append(lobbys, lobby)
			response = methods.CreateLobbyResponse(pin, clientId)
			writeToClient(conn, messageType, &response)
		}

	}
}

func homePage(w http.ResponseWriter, req *http.Request) {
	ws, err := upgrader.Upgrade(w, req, nil)
	if err != nil {
		panic(err)
	}

	log.Println("Client connected")

	reader(ws)
}

func main() {
	validPins.NewArange(1000, 10000)
	validIds.NewArange(0, 10000)

	http.HandleFunc("/", homePage)
	// http.HandleFunc("/ws", wsEndpoint)

	http.ListenAndServe(":8080", nil)
}
